#include<algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <random>
#include <string>
#include <vector>

#include "../mpc.h"

// konstante Parameter
double timeStep;
int Nx;
int Ny;
int Nz;
int MPCparticles;
double T;
double alpha;
int simTime;
double forceX;
double forceY;
double forceZ;

std::vector<std::string> split(const std::string& s, const std::string& delim, const bool keep_empty = true)
{
    using namespace std;
    vector<string> result;
    if (delim.empty()) {
        result.push_back(s);
        return result;
    }
    string::const_iterator substart = s.begin(), subend;
    while (true) {
        subend = search(substart, s.end(), delim.begin(), delim.end());
        string temp(substart, subend);
        if (keep_empty || !temp.empty()) {
            result.push_back(temp);
        }
        if (subend == s.end()) {
            break;
        }
        substart = subend + delim.size();
    }
    return result;
}

void readParameters()
{
    using namespace std;
    vector<vector<string>> lines;
    string line;
    ifstream parameters("parameter.ini");
    if (parameters.is_open())
    {
	while (getline(parameters, line))
	{
	    lines.push_back(split(line, " "));

	}
	parameters.close();
    }
    else
    {
	cout << "Unable to open file\n";
    }
    timeStep = stod(lines[0][1]);
    Nx = stod(lines[1][1]);
    Ny = stod(lines[2][1]);
    Nz = stod(lines[3][1]);
    MPCparticles = stod(lines[4][1]);
    T = stod(lines[5][1]);
    alpha = stod(lines[6][1]) * 2 * M_PI / 360.;
    simTime = stod(lines[7][1]);
    forceX = stod(lines[8][1]);
    forceY = stod(lines[9][1]);
    forceZ = stod(lines[10][1]);
}

void printEnergyMomVel(std::vector<Eigen::Vector3d> momArray, double *energyArray, double ***meanVcm)
{
    FILE *fileE = fopen("Daten/energy.csv", "w");
    FILE *fileMom = fopen("Daten/momentum.csv", "w");
    for (int t = 0; t < simTime; t++)
    {
	fprintf(fileE, "%d %.15f\n", t, energyArray[t]);
	fprintf(fileMom, "%.15f %.15f %.15f\n", momArray[t][0], momArray[t][1], momArray[t][2]);
    }
    fclose(fileE);
    fclose(fileMom);

    FILE *fileMean = fopen("Daten/mean.csv", "w");
    for (int x = 0; x < Nx; x++)
    {
	for (int y = 0; y < Ny + 1; y++)
	{
	    fprintf(fileMean, "%d %d %.15f %.15f\n", x, y, meanVcm[x][y][0], meanVcm[x][y][1]);
	}
    }
    fclose(fileMean);
}

void printMeanVel3d(double ****meanVcm3d)
{
    FILE *fileMean = fopen("Daten/mean3d.csv", "w");
    for (int x = 0; x < Nx + 1; x++)
    {
	for (int y = 0; y < Ny + 1; y++)
	{
	    for (int z = 0; z < Nz + 1; z++)
	    {
		fprintf(fileMean, "%d %d %d %.15f %.15f %.15f\n", x, y, z, meanVcm3d[x][y][z][0], meanVcm3d[x][y][z][1], meanVcm3d[x][y][z][2]);
	    }
	}
    }
    fclose(fileMean);
}

int main()
{
    readParameters();
    std::vector<Eigen::Vector3d> momArray(simTime);

    double *energyArray = new double[simTime]();

    double ***meanVcm = new double**[Nx + 1]();
    for (int x = 0; x < Nx; x++)
    {
        meanVcm[x] = new double*[Ny + 1]();
        for (int y = 0; y < Ny + 1; y++)
        {
            meanVcm[x][y] = new double[3]();
        }
    }

    double ****meanVcm3d = new double***[Nx + 1]();
    for (int x = 0; x < Nx + 1; x++)
    {
        meanVcm3d[x] = new double**[Ny + 1]();
        for (int y = 0; y < Ny + 1; y++)
        {
            meanVcm3d[x][y] = new double*[Nz + 1]();
            for (int z = 0; z < Nz + 1; z++)
            {
		meanVcm3d[x][y][z] = new double[3]();
	    }
        }
    }

    unsigned int seed = std::random_device()();
    printf("seed: %u\n", seed);
    std::vector<std::mt19937> generators(omp_get_max_threads());
    for (size_t i = 0; i < generators.size(); i++)
    {
    	generators[i] = std::mt19937(seed + i);
    }

    Mpc system = Mpc(timeStep, Nx, Ny, Nz, MPCparticles, 0, T, alpha, false, true, false, 1, 1, generators);

    Eigen::Vector3d force;
    force[0] = forceX;
    force[1] = forceY;
    force[2] = forceZ;
    for (int t = 0; t < simTime; t++)
    {
    	system.reset_Parameter();
    	system.v_center_of_mass();
    	system.MCthermostat(0.05);
    	system.collision();
    	system.streaming(force);

	system.printVcm(meanVcm, simTime);
//	system.printVcm3d(meanVcm3d, simTime);
    	momArray[t] = system.momentum();
    	energyArray[t] = system.kinE();
    }
    printEnergyMomVel(momArray, energyArray, meanVcm);
    printMeanVel3d(meanVcm3d);
    return 0;
}
