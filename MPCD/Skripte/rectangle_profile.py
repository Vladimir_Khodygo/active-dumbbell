import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

x, y, vz = np.loadtxt("Daten/mean.csv", unpack=True)
vzMatrix = np.array_split(vz, x[-1] + 1)

_, parameters = np.genfromtxt("parameter.ini", unpack=True)
Nx, Ny, Nz = parameters[1:4]
eta = 8
MPCparticles = parameters[4]
deltaP = parameters[-1] * MPCparticles / (Nx * Ny * Nz)

def fourierSeries(nmax, x, y, Ny, Nx, Nz, deltaP, eta):
    series = [1 / n**3 * (1 - np.cosh(n * np.pi * (x - 0.5 * Nx) / Ny) / np.cosh(n * np.pi * Nx / (2 * Ny))) * np.sin(n * np.pi * y / Ny) for n in range(1, nmax, 2)]
    return 4 * Ny**2 * deltaP / (np.pi**3 * eta) * sum(series)

fig1 = plt.figure()
ax1 = fig1.add_subplot(1, 1, 1, projection="3d")
ax1.scatter(x, y, vz, color="r", marker="+")
ax1.scatter(x, y, fourierSeries(10, x, y, Nx, Ny, Nz, deltaP, eta), color="b", marker="x")
fig1.savefig("Plots/rectangular_scatter.pdf")

fig2 = plt.figure()
ax2 = fig2.add_subplot(1, 1, 1)
ax2.imshow(vzMatrix, origin="lower")

fig2.savefig("Plots/rectangular.pdf")
