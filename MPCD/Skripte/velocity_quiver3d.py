import numpy as np
from mayavi import mlab

x, y, z, vx, vy, vz = np.loadtxt("Daten/mean3d.csv", unpack=True)

fig = mlab.figure(1, size=(5000, 5000), bgcolor=(0, 0, 0), fgcolor=(0, 0, 1))

mlab.quiver3d(x, y, z, vx, vy, vz, line_width=5, scale_factor=0.65, mode="arrow")

mlab.show()
#mlab.savefig("Plots/quiver.png")
