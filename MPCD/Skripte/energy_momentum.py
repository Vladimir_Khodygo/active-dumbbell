import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

momentumSim = np.loadtxt("Daten/momentum.csv")
t, kinESim = np.loadtxt("Daten/energy.csv", unpack=True)


fig2 = plt.figure()
ax = fig2.add_subplot(3, 1, 1)
ax.plot(t, momentumSim[:,0], "r--")
ax = fig2.add_subplot(3, 1, 2)
ax.plot(t, momentumSim[:,1], "r--")
ax = fig2.add_subplot(3, 1, 3)
ax.plot(t, momentumSim[:,2], "r--")
fig2.savefig("Plots/momentum.pdf")

fig4 = plt.figure()
ax = fig4.add_subplot(1, 1, 1)
ax.set_ylim(1.45, 1.55)
ax.plot(t, kinESim, "r--")
fig4.savefig("Plots/kinE.pdf")
