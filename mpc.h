#include <cmath>
#include <Eigen/Dense>
#include <vector>

#ifndef MPC_H
#define MPC_H

/* Vorausgesetzte Parameter:
 * a = 1
 * k = 1
*/

class Mpc
{
    private:
    	// Systemparameter
    	const double timeStep_; // Zeit zwischen Kollisionen
    	const int Nx_, Ny_, Nz_; // Abmessungen des Systems
    	const int MPCparticles_; // Anzahl der Teilchen
    	const int MDparticles_; // Anzahl der Teilchen
    	const double T_; // Temperatur des Systems
    	const double calpha_; // Rotationswinkel
    	const double salpha_; // Rotationswinkel
    	const bool wallX_; // Wand bei x(0) und x(h)
    	const bool wallY_; // Wand bei y(0) und y(h)
    	const bool wallZ_; // Wand bei z(0) und z(h)
    	const double massMPC_, massMD_;
    	const double M_; // mittlere Teilchenzahl / Zelle
	
	// Arrays für Gittergrößen
	// könnte man auch noch in vector oder smart-pointer umschreiben
	int ***MPCParticlesPerCell_; // #MPC teilchen / Array
	int ***MDParticlesPerCell_; // #MD teilchen / Array
	double ****vcm_; // center of mass velocities
	double ****R_; // random numbers for rotation
	double shift_[3]; // grid shifting
	double ***relEkin_; // relative kin. Energien
	double ***kappa_; // Skalierungsfaktor
	
	// Teilchenparameter
	std::vector<Eigen::Vector3d> v_;
	std::vector<Eigen::Vector3d> r_;
	std::vector<Eigen::Vector3d> xyz_;

	std::vector<std::mt19937> generators_;

    public:
    	Mpc(double const timeStep,
    	    const int Nx, const int Ny, const int Nz,
    	    const int MPCparticles,
    	    const int MDparticles,
    	    const double T,
    	    const double alpha,
    	    const bool wallX, const bool wallY, const bool wallZ,
    	    const double massMPC, const double massMD,
    	    std::vector<std::mt19937> &generators); // Konstruktor
    	// Algorithmus
    	void reset_Parameter(); // alle Parameter auf 0 setzen
    	void v_center_of_mass(); // mittlere Geschw. berechnen
    	void collision(); // Kollisionsschritt
    	void streaming(Eigen::Vector3d force); // Streaming-Schritt
    	void ghostParticles(int x, int y, int z); // Ghost-Particles für RB
	int boundary(double pos, double shift, bool wall, int N); // period. RB und shift
	void bounce_back(int i, double deltah, Eigen::Vector3d force);
	int noSlip(int i, Eigen::Vector3d force); // no-Slip RB
	void relativeEkin();
    	double isokin_thermostat(int x, int y, int z); // Thermostat
    	void isotherm_thermostat(); // Thermostat
    	void MCthermostat(double c);
    	void propulsion(double alpha);

    	//Get- / Set-Funktionen
    	void setMDparticles(std::vector<Eigen::Vector3d> &r, std::vector<Eigen::Vector3d> &v);
	std::vector<Eigen::Vector3d> getMDparticles();

	// Measurements
    	double kinE();
	Eigen::Vector3d momentum();
    	void printVelocities(int t);
    	void printVcm(double ***meanVel, int simTime);
    	void printVcm3d(double ****meanVel3d, int simTime);
    	~Mpc(); // DESTRUCTION
};

#endif
