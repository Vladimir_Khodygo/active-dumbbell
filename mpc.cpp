#include <algorithm>
#include <cmath>
#include <functional>
#include <omp.h>
#include <random>
#include <iostream>

#include "mpc.h"

// Konstruktor -------------------------------------------------------------------------
Mpc::Mpc(const double timeStep,
	 const int Nx, const int Ny, const int Nz,
	 const int MPCparticles,
	 const int MDparticles,
	 const double T,const double alpha,
	 const bool wallX, const bool wallY, const bool wallZ,
	 const double massMPC, const double massMD,
	 std::vector<std::mt19937> &generators) :
    timeStep_(timeStep),
    Nx_(wallX ? Nx + 1 : Nx),
    Ny_(wallY ? Ny + 1 : Ny),
    Nz_(wallZ ? Nz + 1 : Nz),
    MPCparticles_(MPCparticles),
    MDparticles_(MDparticles),
    T_(T),
    calpha_(cos(alpha)),
    salpha_(sin(alpha)),
    wallX_(wallX), wallY_(wallY), wallZ_(wallZ),
    massMPC_(massMPC), massMD_(massMD),
    M_((MPCparticles + MDparticles) / (Nx * Ny * Nz)),
    v_(MPCparticles + MDparticles),
    r_(MPCparticles + MDparticles),
    xyz_(MPCparticles + MDparticles),
    generators_(generators)
{
    MPCParticlesPerCell_ = new int**[Nx_]();
    MDParticlesPerCell_ = new int**[Nx_]();
    vcm_ = new double***[Nx_]();
    R_ = new double***[Nx_]();
    relEkin_ = new double**[Nx_]();
    kappa_ = new double**[Nx_]();
    for (int x = 0; x < Nx_; x++)
    {
    	MPCParticlesPerCell_[x] = new int*[Ny_]();
    	MDParticlesPerCell_[x] = new int*[Ny_]();
    	vcm_[x] = new double**[Ny_]();
    	R_[x] = new double**[Ny_]();
    	relEkin_[x] = new double*[Ny_]();
    	kappa_[x] = new double*[Ny_]();
	for (int y = 0; y < Ny_; y++)
	{
    	    MPCParticlesPerCell_[x][y] = new int[Nz_]();
    	    MDParticlesPerCell_[x][y] = new int[Nz_]();
    	    vcm_[x][y] = new double*[Nz_]();
    	    R_[x][y] = new double*[Nz_]();
    	    relEkin_[x][y] = new double[Nz_]();
    	    kappa_[x][y] = new double[Nz_]();
	    for (int z = 0; z < Nz_; z++)
	    {
    	    	vcm_[x][y][z] = new double[3]();
    	    	R_[x][y][z] = new double[3]();
	    }
	}
    }

    // random generators
    double center_of_massX = 0;
    double center_of_massY = 0;
    double center_of_massZ = 0;
    #pragma omp parallel
    {
	std::normal_distribution<double> normal_distribution(0., 1.);
	std::uniform_real_distribution<double> uniform_distribution(0, 1.);
	auto normal = bind(normal_distribution, ref(generators_[omp_get_thread_num()]));
	auto uniform = bind(uniform_distribution, ref(generators_[omp_get_thread_num()]));

	#pragma omp for reduction(+:center_of_massX, center_of_massY, center_of_massZ)
	for (int i = 0; i < MPCparticles_; i++)
	{
	    v_[i][0] = sqrt(T_) * normal();
	    v_[i][1] = sqrt(T_) * normal();
	    v_[i][2] = sqrt(T_) * normal();
	    center_of_massX += v_[i][0] / MPCparticles_;
	    center_of_massY += v_[i][1] / MPCparticles_;
	    center_of_massZ += v_[i][2] / MPCparticles_;

	    r_[i][0] = (wallX_ ? Nx_ - 1 : Nx_) * uniform();
	    r_[i][1] = (wallY_ ? Ny_ - 1 : Ny_) * uniform();
	    r_[i][2] = (wallZ_ ? Nz_ - 1 : Nz_) * uniform();
	}

	// Schwerpunktsgeschw. abziehen
	#pragma omp for
	for (int i = 0; i < MPCparticles_; i++)
	{
	    v_[i][0] -= center_of_massX;
	    v_[i][1] -= center_of_massY;
	    v_[i][2] -= center_of_massZ;
	}
    }
}

// Reset Parameters --------------------------------------------------------------------
void Mpc::reset_Parameter()
{
    #pragma omp parallel
    {
    	std::uniform_real_distribution<double> distribution(0.,1.);
    	std::uniform_real_distribution<double> distribution2(-1.,1.);
    	auto Mersenne = bind(distribution, ref(generators_[omp_get_thread_num()]));
    	auto Mersenne2 = bind(distribution2, ref(generators_[omp_get_thread_num()]));

    	#pragma omp for collapse(3)
    	for (int x = 0; x < Nx_; x++)
    	{
	    for (int y = 0; y < Ny_; y++)
	    {
	    	for (int z = 0; z < Nz_; z++)
	    	{
		    double theta = Mersenne2();
		    double phi = Mersenne();
	    	    MPCParticlesPerCell_[x][y][z] = 0;
	    	    MDParticlesPerCell_[x][y][z] = 0;
	    	    vcm_[x][y][z][0] = 0;
	    	    vcm_[x][y][z][1] = 0;
	    	    vcm_[x][y][z][2] = 0;

	    	    R_[x][y][z][0] = sqrt(1 - theta * theta) * cos(2 * M_PI * phi);
	    	    R_[x][y][z][1] = sqrt(1 - theta * theta) * sin(2 * M_PI * phi);
	    	    R_[x][y][z][2] = theta;
	    	    relEkin_[x][y][z] = 0;
	    	}
	    }
	 }

	#pragma omp single
	{
    	    shift_[0] = 0.5 - Mersenne();
    	    shift_[1] = 0.5 - Mersenne();
    	    shift_[2] = 0.5 - Mersenne();
    	}
    }
}

// Center-of-Mass-Velocities -----------------------------------------------------------
void Mpc::v_center_of_mass()
{
    for (int i = 0; i < MPCparticles_ + MDparticles_; i++)
    {
	int x, y, z;
    	x = xyz_[i][0] = boundary(r_[i][0], shift_[0], wallX_, Nx_),
	y = xyz_[i][1] = boundary(r_[i][1], shift_[1], wallY_, Ny_),
	z = xyz_[i][2] = boundary(r_[i][2], shift_[2], wallZ_, Nz_);

	if (i < MPCparticles_)
	{
	    MPCParticlesPerCell_[x][y][z]++;
	    vcm_[x][y][z][0] += massMPC_ * v_[i][0];
	    vcm_[x][y][z][1] += massMPC_ * v_[i][1];
	    vcm_[x][y][z][2] += massMPC_ * v_[i][2];
	}
	else
	{
	    MDParticlesPerCell_[x][y][z]++;
	    vcm_[x][y][z][0] += massMD_ * v_[i][0];
	    vcm_[x][y][z][1] += massMD_ * v_[i][1];
	    vcm_[x][y][z][2] += massMD_ * v_[i][2];
	}
    }

    #pragma omp parallel for collapse(3)
    for (int x = 0; x < Nx_; x++)
    {
	for (int y = 0; y < Ny_; y++)
	{
	    for (int z = 0; z < Nz_; z++)
	    {

		bool borderX = (x == 0 || x == Nx_ - 1) && M_ > (MPCParticlesPerCell_[x][y][z] + MDParticlesPerCell_[x][y][z]);
		bool borderY = (y == 0 || y == Ny_ - 1) && M_ > (MPCParticlesPerCell_[x][y][z] + MDParticlesPerCell_[x][y][z]);
		bool borderZ = (z == 0 || z == Nz_ - 1) && M_ > (MPCParticlesPerCell_[x][y][z] + MDParticlesPerCell_[x][y][z]);
		bool ghost = (wallX_ && borderX) || (wallY_ && borderY) || (wallZ_ && borderZ);
	    	if (ghost)
	    	{
	    	    ghostParticles(x, y, z);
	    	}
	    	else if ((MPCParticlesPerCell_[x][y][z] + MDParticlesPerCell_[x][y][z]) > 0)
	    	{
	    	    vcm_[x][y][z][0] /= (massMPC_ * MPCParticlesPerCell_[x][y][z] + massMD_ * MDParticlesPerCell_[x][y][z]);
	    	    vcm_[x][y][z][1] /= (massMPC_ * MPCParticlesPerCell_[x][y][z] + massMD_ * MDParticlesPerCell_[x][y][z]);
	    	    vcm_[x][y][z][2] /= (massMPC_ * MPCParticlesPerCell_[x][y][z] + massMD_ * MDParticlesPerCell_[x][y][z]);
	    	}
	    }
	}
    }

}

// collision ---------------------------------------------------------------------------
void Mpc::collision()
{
    #pragma omp parallel for
    for (int i = 0; i < MPCparticles_ + MDparticles_; i++)
    {
    	int x = xyz_[i][0],
	    y = xyz_[i][1],
	    z = xyz_[i][2];

	double R_x = R_[x][y][z][0],
	       R_y = R_[x][y][z][1],
	       R_z = R_[x][y][z][2];

	Eigen::Vector3d v_mittel;
	v_mittel[0] = vcm_[x][y][z][0];
	v_mittel[1] = vcm_[x][y][z][1];
        v_mittel[2] = vcm_[x][y][z][2];

    	//Elemente der Matrix D_rot:
	Eigen::Matrix3d D_rot;
    	D_rot(0, 0) = R_x*R_x+(1.0-R_x*R_x)*calpha_;
    	D_rot(0, 1) = R_x*R_y*(1.0-calpha_) - R_z*salpha_;
    	D_rot(0, 2) = R_x*R_z*(1.0-calpha_) + R_y*salpha_;
    	D_rot(1, 0) = R_x*R_y*(1.0-calpha_) + R_z*salpha_;
    	D_rot(1, 1) = R_y*R_y+(1.0-R_y*R_y)*calpha_;
    	D_rot(1, 2) = R_y*R_z*(1.0-calpha_) - R_x*salpha_;
    	D_rot(2, 0) = R_x*R_z*(1.0-calpha_) - R_y*salpha_;
    	D_rot(2, 1) = R_y*R_z*(1.0-calpha_) + R_x*salpha_;
    	D_rot(2, 2) = R_z*R_z+(1.0-R_z*R_z)*calpha_;

	double kappa = kappa_[x][y][z];
    	v_[i] = v_mittel + kappa * D_rot * (v_[i] - v_mittel);
    }
}

// streaming ---------------------------------------------------------------------------
void Mpc::streaming(Eigen::Vector3d force)
{
    #pragma omp parallel for
    for (int i = 0; i < MPCparticles_; i++)
    {
	r_[i] += v_[i] * timeStep_ + 0.5 * timeStep_ * timeStep_ / massMPC_ * force;
	v_[i] +=  force * timeStep_ / massMPC_;
    	while (noSlip(i, force) > 0){}
    }
}

// ghost-Particles ---------------------------------------------------------------------
void Mpc::ghostParticles(int x, int y, int z)
{
    using namespace std;
    double sigma = sqrt((M_ - (MPCParticlesPerCell_[x][y][z] + MDParticlesPerCell_[x][y][z])) * T_);
    normal_distribution<double> normal_distribution(0, sigma);
    auto normal = bind(normal_distribution, ref(generators_[omp_get_thread_num()]));
    vcm_[x][y][z][0] = (vcm_[x][y][z][0] + normal()) / M_;
    vcm_[x][y][z][1] = (vcm_[x][y][z][1] + normal()) / M_;
    vcm_[x][y][z][2] = (vcm_[x][y][z][2] + normal()) / M_;
}

// periodic Boundary -------------------------------------------------------------------
int Mpc::boundary(double pos, double shift, bool wall, int N)
{
    pos += shift;
    if (wall)
    {
    	if (shift < 0)
    	{
    	    return int(pos) + 1;
        }
        else
        {
            return int(pos);
        }
    }
    else
    {
    	return int(pos - N * floor(pos / N));
    }
}

// bounce back boundary ----------------------------------------------------------------
void Mpc::bounce_back(int i, double deltah, Eigen::Vector3d force)
{
	r_[i] += - 2 * v_[i] * deltah + 2 * force * deltah * deltah / massMPC_;
	v_[i] = -v_[i] + 2 * force * deltah / massMPC_;
}

//  noSlip -----------------------------------------------------------------------------
int Mpc::noSlip(int i, Eigen::Vector3d force)
{
    std::array<bool, 3> borders = {wallX_ && (r_[i][0] < 0 || r_[i][0] > (Nx_ - 1)),
    	                           wallY_ && (r_[i][1] < 0 || r_[i][1] > (Ny_ - 1)),
    	                           wallZ_ && (r_[i][2] < 0 || r_[i][2] > (Nz_ - 1))};

    double deltah[3] = {(r_[i][0] - (Nx_ - 1) * (r_[i][0] - (Nx_ - 1) >= 0)) / v_[i][0],
    	                (r_[i][1] - (Ny_ - 1) * (r_[i][1] - (Ny_ - 1) >= 0)) / v_[i][1],
    	                (r_[i][2] - (Nz_ - 1) * (r_[i][2] - (Nz_ - 1) >= 0)) / v_[i][2]};

    int wallHits = std::count_if(borders.begin(), borders.end(), [](bool x){return x;});
    if (wallHits == 0)
    {
    	return wallHits;
    }
    else if (wallHits == 1)
    {
    	if (borders[0])
    	{
    	    bounce_back(i, deltah[0], force);
    	}
    	else if (borders[1])
    	{
    	    bounce_back(i, deltah[1], force);
    	}
    	else
    	{
    	    bounce_back(i, deltah[2], force);
    	}
	return wallHits;
    }
    else
    {
	// Berechnung der Abstände zu den Schnittpunkten
	double r[3] = {(v_[i] * deltah[0] + force * deltah[0] * deltah[0] / massMPC_).norm(),
	               (v_[i] * deltah[1] + force * deltah[1] * deltah[1] / massMPC_).norm(),
	               (v_[i] * deltah[2] + force * deltah[2] * deltah[2] / massMPC_).norm()};

	//bubble sort
    	for (int i = 3; i > 0; i--)
    	{
    	    for (int j = 0; j < i - 1; j++)
    	    {
    	    	if (r[j] > r[j + 1])
    	    	{
    	    	    double tmp_r = r[j];
    	    	    r[j] = r[j + 1];
    	    	    r[j + 1] = tmp_r;

    	    	    bool tmp_border = borders[j];
    	    	    borders[j] = borders[j + 1];
    	    	    borders[j + 1] = tmp_border;

    	    	    double tmp_deltah = deltah[j];
    	    	    deltah[j] = deltah[j + 1];
    	    	    deltah[j + 1] = tmp_deltah;
    	    	}
    	    }
    	}

	int comp = 2;
	while (!borders[comp] && comp >= 0)
	{
	    comp--;
	}
	bounce_back(i, deltah[comp], force);

	return wallHits;
    }
}

// Thermostate --------------------------------------------------------------------------
// gibt Skalierungsfaktor kappa zurück
double Mpc::isokin_thermostat(int x, int y, int z)
{
    if (relEkin_[x][y][z] == 0)
    {
    	return 1;
    }
    else
    {
    	return sqrt(3 * (MPCParticlesPerCell_[x][y][z] + MDParticlesPerCell_[x][y][z] - 1) * T_ / (2 * relEkin_[x][y][z]));
    }
}

void Mpc::relativeEkin()
{
    // relative kinetische Energie
    for (int i = 0; i < MPCparticles_ + MDparticles_; i++)
    {
    	int x = xyz_[i][0];
    	int y = xyz_[i][1];
    	int z = xyz_[i][2];
    	if (i < MPCparticles_)
    	{
	   relEkin_[x][y][z] += 0.5 * massMPC_ * (pow(v_[i][0] - vcm_[x][y][z][0], 2)
    		                          + pow(v_[i][1] - vcm_[x][y][z][1], 2)
    		                          + pow(v_[i][2] - vcm_[x][y][z][2], 2));
    	}
    	else
    	{
	   relEkin_[x][y][z] += 0.5 * massMD_ * (pow(v_[i][0] - vcm_[x][y][z][0], 2)
    		                          + pow(v_[i][1] - vcm_[x][y][z][1], 2)
    		                          + pow(v_[i][2] - vcm_[x][y][z][2], 2));
    	}
    }
}

// speichert Skalierungsfaktor kappa in kappa_[][]
void Mpc::isotherm_thermostat()
{
    relativeEkin();

    // kappa_Faktor für thermostat ausrechnen
    #pragma omp parallel for collapse(3)
    for (int x = 0; x < Nx_; x++)
    {
    	for (int y = 0; y < Ny_; y++)
    	{
    	    for (int z = 0; z < Nz_; z++)
    	    {
    		std::gamma_distribution<double> distribution3(3. / 2 * (MPCParticlesPerCell_[x][y][z] + MDParticlesPerCell_[x][y][z] - 1.), T_);
    		auto Gamma = bind(distribution3, ref(generators_[omp_get_thread_num()]));
    		if (relEkin_[x][y][z] == 0)
    		{
    		    kappa_[x][y][z] = 1;
    		}
    		else
    		{
    		    kappa_[x][y][z] = sqrt(2 * Gamma() / (2 * relEkin_[x][y][z]));
    		}
    	    }
    	}
    }
}

void Mpc::MCthermostat(double c)
{
    relativeEkin();

    #pragma omp parallel
    {
	// random number [1, 1 + c]
	std::uniform_real_distribution<double> distribution4(1, 1 + c);
	auto uniform = bind(distribution4, ref(generators_[omp_get_thread_num()]));

	// random number 0 or 1
	std::uniform_int_distribution<int> distribution5(0, 1);
	auto boolian = bind(distribution5, ref(generators_[omp_get_thread_num()]));

	// random number [0, 1]
	std::uniform_real_distribution<double> distribution6(0, 1);
	
	#pragma omp for collapse(3)
	for (int x = 0; x < Nx_; x++)
	{
	    for (int y = 0; y < Ny_; y++)
	    {
		for (int z = 0; z < Nz_; z++)
		{
		    auto xi = bind(distribution6, ref(generators_[omp_get_thread_num()]));
		
		    double S = (boolian()) ? uniform() : 1. / uniform();

		    double A = pow(S, 3 * (MPCParticlesPerCell_[x][y][z] - 1)) * exp(- 1 / T_ * (pow(S, 2) - 1) * relEkin_[x][y][z]);

		    if (xi() < std::min(1., A))
		    {
			kappa_[x][y][z] = S;
		    }
		    else
		    {
			kappa_[x][y][z] = 1;
		    }
		}
	    }
	}
    }
}

// propulsion ----------------------------------------------------------------------------
void Mpc::propulsion(double alpha)
{
    std::vector<Eigen::Vector3d> diff(MDparticles_ / 2);
    for (unsigned int i = 0; i < diff.size(); i++)
    {
	Eigen::Vector3d rMD = xyz_[MPCparticles_ + 2 * i];
    	diff[i] = (r_[MPCparticles_ + 2 * i + 1] - r_[MPCparticles_ + 2 * i]).normalized();
    	v_[MPCparticles_ + 2 * i] += alpha * MPCParticlesPerCell_[int(rMD[0])][int(rMD[1])][int(rMD[2])] / massMD_ * diff[i];
    }

    for (int i = 0; i < MPCparticles_; i++)
    {
    	int powerOnOff = -1;
    	for (int j = 0; j < MDparticles_; j += 2)
    	{
	    Eigen::Vector3d rMD = xyz_[MPCparticles_ + j];
    	    if (int(xyz_[i][0]) == int(rMD[0]) && int(xyz_[i][1]) == int(rMD[1]) && int(xyz_[i][2]) == int(rMD[2]))
    	    {
    	    	powerOnOff = j / 2;
    	    	break;
    	    }
    	}
    	if (powerOnOff >= 0)
    	{
    	    v_[i] -= alpha * diff[powerOnOff];
    	}
    }
}

// set- und get-Fkten --------------------------------------------------------------------
void Mpc::setMDparticles(std::vector<Eigen::Vector3d> &r, std::vector<Eigen::Vector3d> &v)
{
    #pragma omp parallel for
    for (int i = MPCparticles_; i < (MPCparticles_ + MDparticles_); i++)
    {
    	r_[i] = r[i - MPCparticles_];
    	v_[i] = v[i - MPCparticles_];
    }
}

std::vector<Eigen::Vector3d>Mpc::getMDparticles()
{
    std::vector<Eigen::Vector3d> vtmp(MDparticles_);
    #pragma omp parallel for
    for (size_t i = 0; i < vtmp.size(); i++)
    {
    	vtmp[i] = v_[i + MPCparticles_];
    }
    return vtmp;
}

//  kinet. Energie -----------------------------------------------------------------------
double Mpc::kinE()
{
    double kinE = 0;
    #pragma omp parallel for reduction(+:kinE)
    for (int i = 0; i < MPCparticles_ + MDparticles_; i++)
    {
    	if (i < MPCparticles_)
    	{
    	    kinE += massMPC_ * (pow(v_[i][0], 2) + pow(v_[i][1], 2) + pow(v_[i][2], 2));
    	}
    	else
    	{
    	    kinE += massMD_  * (pow(v_[i][0], 2) + pow(v_[i][1], 2) + pow(v_[i][2], 2));
    	}
    }
    return 0.5 * kinE / (MPCparticles_ + MDparticles_);
}

//  Impuls -----------------------------------------------------------------------------
Eigen::Vector3d Mpc::momentum()
{
    Eigen::Vector3d mom = Eigen::Vector3d::Zero();
    for (int i = 0; i < MPCparticles_ + MDparticles_; i++)
    {
    	if (i < MPCparticles_)
    	{
	   mom += massMPC_ * v_[i];
	}
	else
	{
	   mom += massMD_ * v_[i];
	}
    }
    return mom;
}

// print-Fkten -------------------------------------------------------------------------
void Mpc::printVelocities(int t)
{
    char velocities[100];
    char positions[100];
    sprintf(velocities, "Daten/Distribution/velocities-%d.csv", t);
    sprintf(positions, "Daten/Distribution/positions-%d.csv", t);
    FILE *fileV = fopen(velocities, "w");
    FILE *fileP = fopen(positions, "w");
    for (int i = 0; i < MPCparticles_; i++)
    {
    	fprintf(fileV, "%.22f %.22f %.22f\n", v_[i][0], v_[i][1], v_[i][2]);
    	fprintf(fileP, "%.22f %.22f %.22f\n", r_[i][0], r_[i][1], r_[i][2]);
    }
    fclose(fileV);
    fclose(fileP);
}

void Mpc::printVcm(double ***meanVel, int simTime)
{
    for (int x = 0; x < Nx_; x++)
    {
	for (int y = 0; y < Ny_; y++)
	{
	    for (int z = 0; z < Nz_; z++)
	    {
		meanVel[x][y][0] += vcm_[x][y][z][0] / (Nz_ * simTime);
		meanVel[x][y][1] += vcm_[x][y][z][1] / (Nz_ * simTime);
		meanVel[x][y][2] += vcm_[x][y][z][2] / (Nz_ * simTime);
	    }
	}
    }
}

void Mpc::printVcm3d(double ****meanVel3d, int simTime)
{
    for (int x = 0; x < Nx_; x++)
    {
	for (int y = 0; y < Ny_; y++)
	{
	    for (int z = 0; z < Nz_; z++)
	    {
		meanVel3d[x][y][z][0] += vcm_[x][y][z][0] / simTime;
		meanVel3d[x][y][z][1] += vcm_[x][y][z][1] / simTime;
		meanVel3d[x][y][z][2] += vcm_[x][y][z][2] / simTime;
	    }
	}
    }
}

//  Dekonstruktor ----------------------------------------------------------------------
Mpc::~Mpc()
{
    for (int x = 0; x < Nx_; x++)
    {
	for (int y = 0; y < Ny_; y++)
	{
	    for (int z = 0; z < Nz_; z++)
	    {
	    	delete[] vcm_[x][y][z];
	    	delete[] R_[x][y][z];
	    }
	    delete[] MPCParticlesPerCell_[x][y];
	    delete[] MDParticlesPerCell_[x][y];
	    delete[] relEkin_[x][y];
	    delete[] vcm_[x][y];
	    delete[] R_[x][y];
	}
	delete[] MPCParticlesPerCell_[x];
	delete[] MDParticlesPerCell_[x];
	delete[] relEkin_[x];
	delete[] vcm_[x];
	delete[] R_[x];
    }
    delete[] MPCParticlesPerCell_;
    delete[] MDParticlesPerCell_;
    delete[] relEkin_;
    delete[] vcm_;
    delete[] R_;
}
