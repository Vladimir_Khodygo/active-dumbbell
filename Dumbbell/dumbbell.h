#include <Eigen/Dense>
#include <functional>
#include <vector>

#ifndef DUMBBELL_H
#define DUMBBELL_H

class Dumbbell
{
    private:
    	int NumberDumb_;
    	double timeStep_;
    	int Nx_, Ny_, Nz_;
    	double l_;
    	double k_;
    	double cut_;
    	double eps_;
    	double mass_;

	std::function<Eigen::Vector3d(Eigen::Vector3d, double)> LJfield_;
	std::function<Eigen::Vector3d(Eigen::Vector3d, double)> springField_;
	// Potentiale
	std::function<double(double)> LJPot_;
	std::function<double(double)> springPot_;

	std::vector<Eigen::Vector3d> r_;
	std::vector<Eigen::Vector3d> v_;
	std::vector<std::vector<Eigen::Vector3d>> forces_;

    public:
	Dumbbell(int NumberDumb,
		 double timeStep,
    		 int Nx, int Ny, int Nz,
    		 double l,
    		 double kappa,
    		 double eps,
    		 double mass);
    	void calcForces();
	Eigen::Vector3d boundary(Eigen::Vector3d r);
    	void Verlet();
    	double Ekin();
    	double EPot();

	void SetVel(std::vector<Eigen::Vector3d> v);
	std::tuple<std::vector<Eigen::Vector3d>, std::vector<Eigen::Vector3d>> GetPosVel();

	void Output(int t, double prop);
};

#endif
