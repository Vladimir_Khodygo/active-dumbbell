#include <cmath>
#include <iostream>

#include "dumbbell.h"

Dumbbell::Dumbbell(int NumberDumb,
	           double timeStep,
	           int Nx, int Ny, int Nz,
	           double l,
	           double k,
	           double eps,
	           double mass) :
    NumberDumb_(NumberDumb),
    timeStep_(timeStep),
    Nx_(Nx), Ny_(Ny), Nz_(Nz),
    l_(l),
    k_(k),
    cut_(pow(2, 1./6) * l),
    eps_(eps),
    mass_(mass),
    r_(NumberDumb),
    v_(NumberDumb),
    forces_(NumberDumb, std::vector<Eigen::Vector3d>(2))
{
    // Kraftefelder
    LJfield_ = [l, eps](Eigen::Vector3d r, double abs_r)
    {
    	double sigma = l;
        return 24 * eps / (sigma * sigma)  * (2 * pow(sigma / abs_r, 14) - pow(sigma / abs_r, 8)) * r;
    };

    springField_ = [k, l](Eigen::Vector3d r, double abs_r)
    {
    	return  k * (abs_r - l) / abs_r * r;
    };

    // Potentiale
    LJPot_ = [l, eps](double abs_r)
    {
    	double sigma = l;
    	return 4 * eps * (pow(sigma / abs_r, 12) - pow(sigma / abs_r, 6));
    };

    springPot_ = [k, l](double abs_r)
    {
    	return k / 2. * (abs_r - l) * (abs_r - l);
    };

    // Initialisation
    r_[0][0] = Nx / 2.;
    r_[0][1] = Ny / 2.;
    r_[0][2] = Nz / 2.;

    r_[1][0] = Nx / 2.;
    r_[1][1] = Ny / 2. + l;
    r_[1][2] = Nz / 2.;

    v_[0][0] = 0;
    v_[0][1] = 0;
    v_[0][2] = 0;

    v_[1][0] = 0;
    v_[1][1] = 0;
    v_[1][2] = 0;
}

// period. Randbedingungen ---------------------------------------------------------------------
Eigen::Vector3d Dumbbell::boundary(Eigen::Vector3d r)
{
    r[0] -= Nx_ * floor(r[0] / Nx_ + 0.5);
    r[1] -= Ny_ * floor(r[1] / Ny_ + 0.5);
    r[2] -= Nz_ * floor(r[2] / Nz_ + 0.5);

    return r;
}

// Kräfte berechnen --------------------------------------------------------------------
void Dumbbell::calcForces()
{
    // neue Kräfte werden alte Kräfte
    // neue Kräfte = 0 setzen
    for (int i = 0; i < NumberDumb_; i++)
    {
        forces_[i][0] = forces_[i][1];
        forces_[i][1] = Eigen::Vector3d::Zero();
    }

    // LJ-Kräfte
    for (int i = 0; i < NumberDumb_; i++)
    {
    	for (int j = 0; j < i; j++)
    	{
	    Eigen::Vector3d diffLJ = boundary(r_[i] - r_[j]);
	    double abs_rLJ = diffLJ.norm();

	    if (abs_rLJ < cut_)
	    {
		forces_[i][1] += LJfield_(diffLJ, abs_rLJ);
		forces_[j][1] -= LJfield_(diffLJ, abs_rLJ);
	    }
    	}
    }

    // Spring-Forces
    for (int i = 0; i < NumberDumb_; i += 2)
    {
        Eigen::Vector3d diff = r_[i] - r_[i + 1];
        double abs_r = diff.norm();

        forces_[i][1] -= springField_(diff, abs_r);
        forces_[i + 1][1] += springField_(diff, abs_r);
    }
}

// Geschwindigkeits-Verlet-Algorithmus -------------------------------------------------
void Dumbbell::Verlet()
{
    for (int i = 0; i < NumberDumb_; i++)
    {
        r_[i] += v_[i] * timeStep_ + 1. / (2 * mass_) * forces_[i][1] * timeStep_ * timeStep_;

    }
    calcForces();

    for (int i = 0; i < NumberDumb_; i++)
    {
        v_[i] += 1. / (2 * mass_) * (forces_[i][0] + forces_[i][1]) * timeStep_;
    }
}

// kinetische Energie ----------------------------------------------------------------------
double Dumbbell::Ekin()
{
    double Ekin = 0;
    for (int i = 0; i < NumberDumb_; i++)
    {
        Ekin += v_[i].squaredNorm();
    }
    return 0.5 * mass_ * Ekin / NumberDumb_;
}

// potentielle Energie ---------------------------------------------------------------------
double Dumbbell::EPot()
{
    double Epot = 0;
    for (int i = 0; i < NumberDumb_; i++)
    {
    	for (int j = 0; j < i; j++)
    	{
	    Eigen::Vector3d diffLJ = boundary(r_[i] - r_[j]);
	    double abs_rLJ = diffLJ.norm();

	    if (abs_rLJ < cut_)
	    {
		Epot += LJPot_(abs_rLJ) + eps_;
	    }
    	}
    }

    for (int i = 0; i < NumberDumb_ - 1; i++)
    {
        Eigen::Vector3d diff = r_[i] - r_[i + 1];
        double abs_r = diff.norm();

        Epot += springPot_(abs_r);
    }

    return Epot / NumberDumb_;
}

void Dumbbell::SetVel(std::vector<Eigen::Vector3d> v)
{
    for (int i = 0; i < NumberDumb_; i++)
    {
        v_[i] = v[i];
    }
}

// Set & Get-Geschwindigkeiten & Positionen --------------------------------------------------
std::tuple<std::vector<Eigen::Vector3d>, std::vector<Eigen::Vector3d>> Dumbbell::GetPosVel()
{
    std::vector<Eigen::Vector3d> r(NumberDumb_);
    std::vector<Eigen::Vector3d> v(NumberDumb_);
    for (int i = 0; i < NumberDumb_; i++)
    {
        r[i] = r_[i];
        v[i] = v_[i];
    }

    return std::make_tuple(r, v);
}

void Dumbbell::Output(int t, double prop)
{
    char velocities0[100];
    char positions0[100];
    sprintf(velocities0, "Daten/velocities-%.2f-%d.csv", prop, 0);
    sprintf(positions0, "Daten/positions-%.2f-%d.csv", prop, 0);
    char velocities1[100];
    char positions1[100];
    sprintf(velocities1, "Daten/velocities-%.2f-%d.csv", prop, 1);
    sprintf(positions1, "Daten/positions-%.2f-%d.csv", prop, 1);
    FILE *fileV0 = new FILE;
    FILE *fileP0 = new FILE;
    FILE *fileV1 = new FILE;
    FILE *fileP1 = new FILE;
    if (t == 0)
    {
	fileV0 = fopen(velocities0, "w");
	fileP0 = fopen(positions0, "w");
	fileV1 = fopen(velocities1, "w");
	fileP1 = fopen(positions1, "w");
    }
    else
    {
	fileV0 = fopen(velocities0, "a");
	fileP0 = fopen(positions0, "a");
	fileV1 = fopen(velocities1, "a");
	fileP1 = fopen(positions1, "a");
    }
    for (int i = 0; i < NumberDumb_; i++)
    {
    	if (i % 2 == 0)
    	{
	    fprintf(fileV0, "%.22f %.22f %.22f\n", v_[i][0], v_[i][1], v_[i][2]);
	    fprintf(fileP0, "%.22f %.22f %.22f\n", r_[i][0], r_[i][1], r_[i][2]);
    	}
    	else
    	{
	    fprintf(fileV1, "%.22f %.22f %.22f\n", v_[i][0], v_[i][1], v_[i][2]);
	    fprintf(fileP1, "%.22f %.22f %.22f\n", r_[i][0], r_[i][1], r_[i][2]);
    	}
    }
    fclose(fileV0);
    fclose(fileP0);
    fclose(fileV1);
    fclose(fileP1);
}
