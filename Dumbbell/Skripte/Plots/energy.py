import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

t, kinE, Epot, Eges = np.loadtxt("Daten/energy.csv", unpack=True)

fig4 = plt.figure()
ax = fig4.add_subplot(1, 1, 1)
ax.plot(t, kinE, "r--", label="kinE")
ax.set_ylim(0., 5)
ax.plot(t, Epot, "b--", label="Epot")
#ax.plot(t, Eges, "g--", label="Eges")
ax.legend(loc="best")
fig4.savefig("Plots/E.pdf")
